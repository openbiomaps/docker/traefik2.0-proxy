Traefik v2 proxy telepítés lépések:

Előfeltételek:
- [ ] docker telepítve
- [ ] docker-compose telepítve

Telepítés:
- [ ] telepítési könyvtár létrehozása
  útvonal:
- [ ] git clone <repository>
- [ ] docker-compose pull
- [ ] domain név átírása
- [ ] https konfiguráció
- [ ] docker-compose up -d
- [ ] teszt https://domain/whois/

